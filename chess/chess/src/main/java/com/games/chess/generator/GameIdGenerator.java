package com.games.chess.generator;

import java.util.UUID;

public class GameIdGenerator implements IdGenerator {

	@Override
	public String generateId() {
		return UUID.randomUUID().toString().substring(0, 8);
	}

}
