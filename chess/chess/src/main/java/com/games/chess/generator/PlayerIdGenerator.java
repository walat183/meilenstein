package com.games.chess.generator;

import java.util.UUID;

public class PlayerIdGenerator implements IdGenerator {

	@Override
	public String generateId() {
		return UUID.randomUUID().toString();
	}

}
