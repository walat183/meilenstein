package com.games.chess.manager;

import com.games.chess.entities.GameState;
import com.games.chess.entities.GamesRegister;
import com.games.chess.generator.IdGenerator;

public class BasicGameManager implements ChessGamesManager {

	private GamesRegister gamesRegister;
	private IdGenerator playerIdGenerator;
	private IdGenerator gameIdGenerator;

	public BasicGameManager(IdGenerator playerIdGenerator, IdGenerator gameIdGenerator, GamesRegister gamesRegister) {
		this.playerIdGenerator = playerIdGenerator;
		this.gameIdGenerator = gameIdGenerator;
		this.gamesRegister = gamesRegister;
	}

	@Override
	public String registerNewGame() {
		String gameId = gameIdGenerator.generateId();
		gamesRegister.reigisterGame(gameId);
		return gameId;
	}

	@Override
	public String registerNewPlayer(String gameId) {
		String playerId = playerIdGenerator.generateId();
		gamesRegister.registerPlayer(gameId, playerId);
		return playerId;
	}

	@Override
	public GameState getGameState(String gameId, String playerId) {
		GameState gameState = gamesRegister.getGameState(gameId, playerId);
		return gameState;
	}

	@Override
	public void makePlayerMove(String gameId, String playerId, int startX, int startY, int destinationX,
			int destinationY) {
		gamesRegister.makePlayerMove(gameId, playerId, startX, startY, destinationX, destinationY);
	}

}
