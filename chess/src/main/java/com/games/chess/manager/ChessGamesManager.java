package com.games.chess.manager;

import com.games.chess.entities.GameState;

public interface ChessGamesManager {

	String registerNewGame();

	String registerNewPlayer(String gameId);

	GameState getGameState(String gameId, String playerId);

	void makePlayerMove(String gameId, String playerId, int startX, int startY, int destinationX, int destinationY);

}
