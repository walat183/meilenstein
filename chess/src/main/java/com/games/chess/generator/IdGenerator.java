package com.games.chess.generator;

public interface IdGenerator {

	String generateId();

}
