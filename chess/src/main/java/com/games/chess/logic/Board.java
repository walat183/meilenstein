package com.games.chess.logic;

import java.io.Serializable;
import java.util.Iterator;

import com.games.chess.logic.pieces.Bishop;
import com.games.chess.logic.pieces.King;
import com.games.chess.logic.pieces.Knight;
import com.games.chess.logic.pieces.Pawn;
import com.games.chess.logic.pieces.Piece;
import com.games.chess.logic.pieces.Queen;
import com.games.chess.logic.pieces.Rook;

//Das Schachbrett, das alle Schachfiguren und Methoden zum Hinzuf�gen, 
//Entfernen und �berpr�fen von Figuren und Feldern auf dem Brett enth�lt.
public class Board implements Iterable<Position>, Serializable {

	
	private static final long serialVersionUID = 1L;
	private Piece[][] array;
	private Team turn, winner;
	private boolean gameOver;

//	Behandelt Spezialbewegungen (en passant, Rochade etc..).
	MovementHandler movementHandler;

//	Bestimmt, ob es Schach, Schachmatt, Patt oder Unentschieden gibt.
	private WinnerHandler winnerHandler;

	
	private Position kingWhite, kingBlack;

	
	public Board() {
		reset();
	}

//	R�umt das Brett ab und platziert die Standard-Startsteine.
	public void reset() {
		clear();
		loadPieces();
	}

//	Entfernt alle Schachfiguren vom Brett.
	public void clear() {
		turn = Team.WHITE;
		winner = null;
		gameOver = false;
		array = new Piece[8][8];
		movementHandler = new MovementHandler(this);
		winnerHandler = new WinnerHandler(this);
	}

//	L�dt die Figuren in der standardm��igen Schachstartanordnung auf das Brett.
	private void loadPieces() {

		add(new Rook(this, Team.BLACK), new Position(0, 0));
		add(new Knight(this, Team.BLACK), new Position(1, 0));
		add(new Bishop(this, Team.BLACK), new Position(2, 0));
		add(new Queen(this, Team.BLACK), new Position(3, 0));
		add(new Bishop(this, Team.BLACK), new Position(5, 0));
		add(new Knight(this, Team.BLACK), new Position(6, 0));
		add(new Rook(this, Team.BLACK), new Position(7, 0));
		add(new King(this, Team.BLACK), new Position(4, 0));

		for (int i = 0; i < 8; i++) {
			add(new Pawn(this, Team.BLACK, false), new Position(i, 1));
		}

		add(new Rook(this, Team.WHITE), new Position(0, 7));
		add(new Knight(this, Team.WHITE), new Position(1, 7));
		add(new Bishop(this, Team.WHITE), new Position(2, 7));
		add(new Queen(this, Team.WHITE), new Position(3, 7));
		add(new Bishop(this, Team.WHITE), new Position(5, 7));
		add(new Knight(this, Team.WHITE), new Position(6, 7));
		add(new Rook(this, Team.WHITE), new Position(7, 7));
		add(new King(this, Team.WHITE), new Position(4, 7));

		for (int i = 0; i < 8; i++) {
			add(new Pawn(this, Team.WHITE, true), new Position(i, 6));
		}
	}

//	F�gt dem Brett ein St�ck hinzu.
	public void add(Piece piece, Position position) {
		updateKings(piece, position);
		array[position.getY()][position.getX()] = piece;
	}

//	Holt die Figur an einer bestimmten Position.
	public Piece get(Position position) {
		return array[position.getY()][position.getX()];
	}

//	Entfernt ein St�ck vom Brett.
	void delete(Position position) {
		array[position.getY()][position.getX()] = null;
	}

//	Verschiebt die ausgew�hlte Figur an die angegebene Position.
	public void move(Position p, int x, int y) {
		movementHandler.move(p, x, y);
		updateKings(get(p), p);
		winnerHandler.checkIfGameIsOver();
	}

//	Aktualisiert die Referenzen auf die K�nige. Dies dient der Effizienz.
	void updateKings(Piece piece, Position position) {
		if (piece instanceof King && piece.getTeam() == Team.WHITE) {
			kingWhite = position;
		} else if (piece instanceof King && piece.getTeam() == Team.BLACK) {
			kingBlack = position;
		}
	}

//	Verschiebt eine Figur an eine neue Position und entfernt das St�ck an der neuen Position.
	void move(Position location, Position destination) {
		array[destination.getY()][destination.getX()] = array[location.getY()][location.getX()];
		delete(location);
		get(destination).markAsMoved();
		toggleTurn();
	}

	

//	�berpr�ft, ob eine bestimmte Figur eine Position einnehmen kann, 
//	ohne gegen Regeln zu versto�en (z.B. vom Brett zu springen oder einen 
//	Stein aus dem gleichen Team zu schlagen).
	public boolean isValidSpot(Piece piece, Position position) {
		if (!isInBounds(position)) {
			return false;
		} else if (!isEmptySpot(position) && sameTeam(get(position), piece)) {
			return false;
		}
		return true;
	}

//	�berpr�ft, ob ein bestimmtes Feld auf dem Brett leer ist.
	public boolean isEmptySpot(Position position) {
		if (!isInBounds(position)) {
			throw new IndexOutOfBoundsException();
		}
		return array[position.getY()][position.getX()] == null;
	}

//	�berpr�ft, ob zwei Figuren im selben Team sind.
	public boolean sameTeam(Piece p1, Piece p2) {
		return p1.getTeam().equals(p2.getTeam());
	}

//	�berpr�ft, ob eine Stelle vor Feinden sicher ist.
	public boolean safeSpot(Team team, Position destination) {
		boolean result = true;
		boolean dummyUsed = false;
		Rook dummy;

		if (isEmptySpot(destination)) {
			dummyUsed = true;
			dummy = new Rook(this, team);
			add(dummy, destination);
		}

		for (Position pos : this) {
			if (!get(pos).getTeam().equals(team)) {
				Piece.filterAgainstCheckOFF();
				if (kingContainsMove(pos, destination) || pieceContainsMove(pos, destination)) {
					result = false;
					break;
				}
				Piece.filterAgainstCheckON();
			}
		}

		if (dummyUsed) {
			delete(destination);
		}
		Piece.filterAgainstCheckON();
		return result;
	}
	
	private boolean kingContainsMove(Position position, Position destination) {
		return (isKing(get(position)) && ((King) get(position)).getSurroundingSquares(position).contains(destination));
	}
	
	private boolean pieceContainsMove(Position position, Position destination) {
		return !isKing(get(position)) && containsMove(position, destination);
	}
	
	private boolean isKing(Piece piece) {
		return piece instanceof King;
	}
	
	private boolean containsMove(Position position, Position destination) {
		return get(position).getAllPossibleMoves(position).contains(destination);
	}

	void toggleTurn() {
		turn = (turn.equals(Team.WHITE)) ? Team.BLACK : Team.WHITE;
	}

	public Team getTurn() {
		return turn;
	}

//	Pr�ft, welche Position zu einem en passant f�hren w�rde.
	public Position getEnPassant() {
		return movementHandler.getEnPassant();
	}

//	Stellt die Position ein, die zu einem en passant f�hren w�rde.
	public void setEnPassant(Position enPassant) {
		movementHandler.setEnPassant(enPassant);
	}

	Position getKing(Team team) {
		return (team == Team.WHITE) ? kingWhite : kingBlack;
	}

//	Der Iterator f�r das Spielbrett.
	public Iterator<Position> iterator() {
		return new BoardIterator(array);
	}

	public void setTurn(Team team) {
		this.turn = team;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public Team getWinner() {
		if (!gameOver) {
			throw new IllegalStateException("Cannot get winner if game is not over.");
		}

		return winner;
	}

	void setWinner(Team team) {
		gameOver = true;
		winner = team;
	}

//	�berpr�ft, ob eine Figur sich in eine Position bewegen kann, 
//	ohne ein Schach auf den K�nig des eigenen Teams zu bekommen.
	public boolean wouldBeCheck(Position position, Position destination) {
		return winnerHandler.wouldBeCheck(position, destination);
	}

	public void declareWinner() {
		winnerHandler.declareWinner();
	}

//	�berpr�ft, ob eine Stelle au�erhalb der Grenzen des Schachbretts liegt.
	public static boolean isInBounds(Position position) {
		return position.getX() >= 0 && position.getX() <= 7 && position.getY() >= 0 && position.getY() <= 7;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {

				Position pos = new Position(x, y);
				Piece p;

				if (!isEmptySpot(pos)) {
					p = get(pos);
				} else {
					p = null;
				}

				if (p instanceof Rook) {
					sb.append("R ");
				} else if (p instanceof Knight) {
					sb.append("N ");
				} else if (p instanceof Bishop) {
					sb.append("B ");
				} else if (p instanceof Queen) {
					sb.append("Q ");
				} else if (p instanceof King) {
					sb.append("K ");
				} else if (p instanceof Pawn) {
					sb.append("P ");
				} else {
					sb.append("- ");
				}
			}
			sb.append("\n");
		}

		return new String(sb);
	}

}
