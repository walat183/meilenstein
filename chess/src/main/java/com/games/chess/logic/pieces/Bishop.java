package com.games.chess.logic.pieces;

import java.util.HashSet;

import com.games.chess.logic.Board;
import com.games.chess.logic.Position;
import com.games.chess.logic.Team;


public class Bishop extends Piece {

//	konstruktor
	public Bishop(Board board, Team team) {
		super(board, team);
	}

	@Override
	public HashSet<Position> getAllPossibleMoves(Position position) {
		HashSet<Position> hashSet = new HashSet<>();
		loadNorthWest(hashSet, position);
		loadNorthEast(hashSet, position);
		loadSouthWest(hashSet, position);
		loadSouthEast(hashSet, position);
		return hashSet;
	}

//	L�dt alle Positionen, in die sich der L�ufer entlang der Nordwest-Diagonale bewegen kann.
	private void loadNorthWest(HashSet<Position> hashSet, Position position) {
		for (int i = 1; i < 8; i++) {
			Position destination = new Position(position.getX() - i, position.getY() - i);
			if (blocked(hashSet, position, destination)) {
				return;
			}
		}
	}

//	L�dt alle Positionen, in die sich der L�ufer entlang der Nordost-Diagonale bewegen kann.
	private void loadNorthEast(HashSet<Position> hashSet, Position position) {
		for (int i = 1; i < 8; i++) {
			Position destination = new Position(position.getX() + i, position.getY() - i);
			if (blocked(hashSet, position, destination)) {
				return;
			}
		}
	}

//	L�dt alle Positionen, in die sich der L�ufer entlang der S�dwest-Diagonale bewegen kann.
	private void loadSouthWest(HashSet<Position> hashSet, Position position) {
		for (int i = 1; i < 8; i++) {
			Position destination = new Position(position.getX() - i, position.getY() + i);
			if (blocked(hashSet, position, destination)) {
				return;
			}
		}
	}

//	L�dt alle Positionen, in die sich der L�ufer entlang der S�dost-Diagonale bewegen kann.
	private void loadSouthEast(HashSet<Position> hashSet, Position position) {
		for (int i = 1; i < 8; i++) {
			Position destination = new Position(position.getX() + i, position.getY() + i);
			if (blocked(hashSet, position, destination)) {
				return;
			}
		}
	}

	public Bishop clone(Board board) {
		Bishop bishop = new Bishop(board, this.getTeam());
		bishop.moved = this.moved;
		return bishop;
	}

	@Override
	public String toString() {
		return "Bishop";
	}

}
