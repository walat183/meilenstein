package com.games.chess.logic;

import com.games.chess.logic.pieces.Piece;

class Move {

	private Board board;

	private Position position1, position2, position3, position4;
	private Piece piece1, piece2, piece3, piece4;

	private Position enPassant;

//	Die Zust�nde von zwei Positionen auf dem Brett, bevor ein Zug ausgef�hrt wird.
	Move(Board board, Position position, Position destination) {

		this.board = board;

		this.position1 = position;
		this.position2 = destination;

//		Die bewegte Figur wird nie null sein.
		this.piece1 = board.get(position).clone(board);

//		 Das Ziel kann null sein, wenn keine Figur geschlagen wird.
		this.piece2 = (board.get(destination) == null) ? null : board.get(destination).clone(board);
	}

//	Holt die Startposition der Figur, die direkt vom Spieler bewegt wurde.
	Position getPosition1() {
		return position1;
	}

//	Holt die Zielposition der Figur, das direkt vom Spieler bewegt wurde.
	Position getPosition2() {
		return position2;
	}

//	Holt die Figur, die direkt vom Spieler bewegt wurde, bevor es bewegt wurde.
	Piece getPiece1() {
		return piece1;
	}

//	Holt die Figur, die geschlagen wurde.
	Piece getPiece2() {
		return piece2;
	}

//	Speichert die urspr�ngliche Position des Turms, 
//	wenn eine Rochade aufgetreten ist. Oder speichert die 
//	Position des Bauern, der w�hrend eines en passant geschlagen wurde.
	void setPosition3(Position position) {
		this.position3 = position;
		this.piece3 = board.get(position).clone(board);
	}

//	Holt die urspr�ngliche Position des Turms, 
//	wenn eine Rochade geschehen ist. Oder holt die 
//	Position des Bauers, der w�hrend eines en passant geschlagen wurde.
	Position getPosition3() {
		return position3;
	}

//	Holt den Turm, wenn eine Rochade geschehen ist, 
//	in dem Zustand, in dem er sich vor der Rochade befand. 
//	Oder holt den Bauer, wenn en passant geschehen ist.
	Piece getPiece3() {
		return piece3;
	}

//	Speichert die Zielposition des Turms, wenn eine Rochade geschehen ist.
	void setPosition4(Position position) {
		this.position4 = position;
		this.piece4 = (board.get(position) == null) ? null : board.get(position).clone(board);
	}

//	Holt die Zielposition des Turms, wenn eine Rochade geschehen ist.
	Position getPosition4() {
		return position4;
	}

//	Holt den Turm, wenn eine Rochade geschehen ist, in dem Zustand, in dem er sich nach der Rochade befand.
	Piece getPiece4() {
		return piece4;
	}

//	Stellt die Position ein, die zu einem en passant f�hren w�rde, wenn sich ein Bauer dorthin bewegt.
	void setEnPassant(Position enPassant) {
		this.enPassant = (enPassant == null) ? null : enPassant;
	}

//	Holt die Position ein, die zu einem en passant f�hren w�rde, wenn sich ein Bauer dorthin bewegt.
	Position getEnPassant() {
		return enPassant;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Move move = (Move) o;

		if (position1 != null ? !position1.equals(move.position1) : move.position1 != null)
			return false;
		if (position2 != null ? !position2.equals(move.position2) : move.position2 != null)
			return false;
		if (piece1 != null ? !piece1.equals(move.piece1) : move.piece1 != null)
			return false;
		return piece2 != null ? piece2.equals(move.piece2) : move.piece2 == null;

	}

	@Override
	public int hashCode() {
		int result = position1 != null ? position1.hashCode() : 0;
		result = 31 * result + (position2 != null ? position2.hashCode() : 0);
		result = 31 * result + (piece1 != null ? piece1.hashCode() : 0);
		result = 31 * result + (piece2 != null ? piece2.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(piece1);
		sb.append(" (" + position1.getX() + ", " + position1.getY() + ")\t-> ");
		sb.append("(" + position2.getX() + ", " + position2.getY() + ")");
		sb.append(" | EnPassant: " + enPassant);

		if (position3 != null) {
			sb.append(" | " + piece3);
			sb.append(" (" + position3.getX() + ", " + position3.getY() + ")");
		}

		if (position4 != null) {
			sb.append("\t-> (" + position4.getX() + ", " + position4.getY() + ")");

		}

		sb.append("\n");

		return new String(sb);
	}

}
