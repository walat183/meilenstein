package com.games.chess.logic.pieces;

import java.util.HashSet;

import com.games.chess.logic.Board;
import com.games.chess.logic.Position;
import com.games.chess.logic.Team;


public class Pawn extends Piece {

	private boolean directionNorth;

//	konstruktor
	public Pawn(Board board, Team team, boolean directionNorth) {
		super(board, team);
		this.directionNorth = directionNorth;
	}

	@Override
	public HashSet<Position> getAllPossibleMoves(Position position) {

		HashSet<Position> hashSet = new HashSet<>();

		if (directionNorth) {
			movingNorth(hashSet, position);
		} else {
			movingSouth(hashSet, position);
		}

		return hashSet;
	}

//	"movingNorth" l�dt die m�glichen Positionen, in die sich der Bauer bewegen kann,
//	wenn er nach Norden ausgerichtet ist.
	
	private void movingNorth(HashSet<Position> hashSet, Position position) {

		Position oneNorth = new Position(position.getX(), position.getY() - 1);
		Position twoNorth = new Position(position.getX(), position.getY() - 2);
		Position eatLeft = new Position(position.getX() - 1, position.getY() - 1);
		Position eatRight = new Position(position.getX() + 1, position.getY() - 1);

		boolean secondRow = position.getY() == 6;

		moveForward(hashSet, position, oneNorth);

		if (secondRow) {
			moveForward(hashSet, position, twoNorth);
		}

		eat(hashSet, position, eatLeft);
		eat(hashSet, position, eatRight);
	}

//	"movingSouth" l�dt die m�glichen Positionen, in die sich der Bauer bewegen kann,
//	wenn er nach S�den ausgerichtet ist.
	
	private void movingSouth(HashSet<Position> hashSet, Position position) {

		Position oneSouth = new Position(position.getX(), position.getY() + 1);
		Position twoSouth = new Position(position.getX(), position.getY() + 2);
		Position eatLeft = new Position(position.getX() - 1, position.getY() + 1);
		Position eatRight = new Position(position.getX() + 1, position.getY() + 1);

		boolean seventhRow = position.getY() == 1;

		moveForward(hashSet, position, oneSouth);

		if (seventhRow) {
			moveForward(hashSet, position, twoSouth);
		}

		eat(hashSet, position, eatLeft);
		eat(hashSet, position, eatRight);
	}

//	"moveForward" l�dt Positionen in das Add, nachdem es �berpr�ft wurde, ob sie g�ltig sind.
	
	private void moveForward(HashSet<Position> hashSet, Position position, Position destination) {

		if (!isInBounds(destination))
			return;

		// Der Bauer kann nicht �ber eine Figur springen, wenn er sich zwei Felder vorw�rts bewegt.
		if (Math.abs(position.getY() - destination.getY()) > 1) {

			int middle = (position.getY() + destination.getY()) / 2;
			Position pos = new Position(position.getX(), middle);

			if (!isEmptySpot(pos)) {
				return;
			}
		}

		if (isEmptySpot(destination)) {
			add(hashSet, position, destination);
		}
	}

//	"eat" �berpr�ft, ob der Bauer schlagen kann. Dann l�dt diese Positionen.
	private void eat(HashSet<Position> hashSet, Position position, Position destination) {

		if (Board.isInBounds(destination) && !isEmptySpot(destination) && !isSameTeam(destination)) {
			add(hashSet, position, destination);
		} else if (Board.isInBounds(destination) && destination.equals(board.getEnPassant())) {
			add(hashSet, position, destination);
		}

	}

	
	public boolean goingNorth() {
		return directionNorth;
	}

	
	public Pawn clone(Board board) {
		Pawn pawn = new Pawn(board, getTeam(), this.directionNorth);
		pawn.moved = this.moved;
		pawn.directionNorth = this.directionNorth;
		return pawn;
	}

	@Override
	public String toString() {
		return "Pawn";
	}

}
