package com.games.chess.logic.pieces;

import java.util.HashSet;

import com.games.chess.logic.Board;
import com.games.chess.logic.Position;
import com.games.chess.logic.Team;

public class Knight extends Piece {

//	konstruktor
	public Knight(Board board, Team team) {
		super(board, team);
	}

	@Override
	public HashSet<Position> getAllPossibleMoves(Position position) {
		HashSet<Position> hashSet = new HashSet<>();
		loadNorthMoves(hashSet, position);
		loadSouthMoves(hashSet, position);
		loadWestMoves(hashSet, position);
		loadEastMoves(hashSet, position);
		return hashSet;
	}

//	L�dt die Z�ge �ber dem Springer.
	private void loadNorthMoves(HashSet<Position> hashSet, Position position) {
		Position moveUpLeft = new Position(position.getX() - 1, position.getY() - 2);
		Position moveUpRight = new Position(position.getX() + 1, position.getY() - 2);

		if (isValidSpot(this, moveUpLeft)) {
			add(hashSet, position, moveUpLeft);
		}
		if (isValidSpot(this, moveUpRight)) {
			add(hashSet, position, moveUpRight);
		}
	}

//	L�dt die Z�ge unter dem Springer.
	private void loadSouthMoves(HashSet<Position> hashSet, Position position) {
		Position moveDownLeft = new Position(position.getX() - 1, position.getY() + 2);
		Position moveDownRight = new Position(position.getX() + 1, position.getY() + 2);

		if (isValidSpot(this, moveDownLeft)) {
			add(hashSet, position, moveDownLeft);
		}
		if (isValidSpot(this, moveDownRight)) {
			add(hashSet, position, moveDownRight);
		}
	}


//	L�dt die Z�ge links vom Springer.
	private void loadWestMoves(HashSet<Position> hashSet, Position position) {
		Position moveLeftDown = new Position(position.getX() - 2, position.getY() + 1);
		Position moveLeftUp = new Position(position.getX() - 2, position.getY() - 1);

		if (isValidSpot(this, moveLeftDown)) {
			add(hashSet, position, moveLeftDown);
		}
		if (isValidSpot(this, moveLeftUp)) {
			add(hashSet, position, moveLeftUp);
		}
	}

//	L�dt die Z�ge rechts vom Springer.
	private void loadEastMoves(HashSet<Position> hashSet, Position position) {
		Position moveRightDown = new Position(position.getX() + 2, position.getY() + 1);
		Position moveRightUp = new Position(position.getX() + 2, position.getY() - 1);

		if (isValidSpot(this, moveRightDown)) {
			add(hashSet, position, moveRightDown);
		}
		if (isValidSpot(this, moveRightUp)) {
			add(hashSet, position, moveRightUp);
		}
	}

	
	public Knight clone(Board board) {
		Knight knight = new Knight(board, getTeam());
		knight.moved = this.moved;
		return knight;
	}

	@Override
	public String toString() {
		return "Knight";
	}

}
