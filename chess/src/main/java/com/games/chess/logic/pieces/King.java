package com.games.chess.logic.pieces;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.InputMismatchException;

import com.games.chess.logic.Board;
import com.games.chess.logic.Position;
import com.games.chess.logic.Team;


public class King extends Piece {

//	Konstruktor
	public King(Board board, Team team) {
		super(board, team);
	}

	@Override
	public HashSet<Position> getAllPossibleMoves(Position position) {
		HashSet<Position> hashSet = new HashSet<>();
		getSurroundingSquares(position).forEach((pos) -> addMove(hashSet, position, pos));
		castling(hashSet);
		return hashSet;
	}

//	F�gt eine Position hinzu, wenn die Position nicht 
//	zu einem Schach gegen den Spieler f�hrt, der den Zug macht.
	private void addMove(HashSet<Position> hashSet, Position position, Position destination) {
		if (!board.wouldBeCheck(position, destination)) {
			hashSet.add(destination);
		}
	}

//	L�dt die Rochadepositionen, wenn Rochade m�glich ist.
	private void castling(HashSet<Position> hashSet) {

		if (canCastleToTheBottomRight()) {
			hashSet.add(new Position(6, 7));
		}
		if (canCastleToTheTopRight()) {
			hashSet.add(new Position(6, 0));
		}
		if (canCastleToTheBottomLeft()) {
			hashSet.add(new Position(2, 7));
		}
		if (canCastleToTheTopLeft()) {
			hashSet.add(new Position(2, 0));
		}

	}

//	�berpr�ft, ob es m�glich ist, mit dem Turm unten rechts zu rochieren.
	private boolean canCastleToTheBottomRight() {
		Position p = new Position(7, 7);
		return !hasMoved() && isValidRook(p) && isEmptyToTheRightOf(new Position(5, 7), 2)
				&& isSafeToTheRightOf(new Position(4, 7), 3);
	}

//	�berpr�ft, ob es m�glich ist, mit dem Turm oben rechts zu rochieren.
	private boolean canCastleToTheTopRight() {
		Position p = new Position(7, 0);
		return !hasMoved() && isValidRook(p) && isEmptyToTheRightOf(new Position(5, 0), 2)
				&& isSafeToTheRightOf(new Position(4, 0), 3);
	}

//	�berpr�ft, ob es m�glich ist, mit dem Turm unten links zu rochieren.
	private boolean canCastleToTheBottomLeft() {
		Position p = new Position(0, 7);
		return !hasMoved() && isValidRook(p) && isEmptyToTheRightOf(new Position(1, 7), 2)
				&& isSafeToTheRightOf(new Position(2, 7), 3);
	}

//	�berpr�ft, ob es m�glich ist, mit dem Turm oben links zu rochieren.
	private boolean canCastleToTheTopLeft() {
		Position p = new Position(0, 0);
		return !hasMoved() && isValidRook(p) && isEmptyToTheRightOf(new Position(1, 0), 2)
				&& isSafeToTheRightOf(new Position(2, 0), 3);
	}

//	Pr�ft, ob eine bestimmte Position auf dem Schachbrett eine Figur enth�lt, die ein Turm ist.
	private boolean isValidRook(Position position) {
		return !isEmptySpot(position) && isRook(position) && !isRookMoved(position);
	}

//	Pr�ft, ob eine Figur an einer bestimmten Position ein Turm ist. Die Figur 
//	an der Position darf nicht null sein.
	private boolean isRook(Position position) {
		return board.get(position) instanceof Rook;
	}

//	�berpr�ft, ob der Turm an einer bestimmten Position bewegt wurde. Die Figur
//	an der Position darf nicht null sein.
	private boolean isRookMoved(Position position) {
		Piece piece = board.get(position);

		if (!(piece instanceof Rook)) {
			throw new InputMismatchException();
		}

		return (board.get(position)).hasMoved();
	}

//	Pr�ft, ob eine bestimmte Anzahl von Feldern rechts von einer 
//	bestimmten Position keine Steine enth�lt. (inklusiv die Position selbst)
	private boolean isEmptyToTheRightOf(Position position, int squaresToCheck) {
		ArrayList<Position> list = new ArrayList<>();

		for (int i = 0; i < squaresToCheck; i++) {
			list.add(new Position(position.getX() + i, position.getY()));
		}

		for (Position p : list) {
			if (!isEmptySpot(p)) {
				return false;
			}
		}

		return true;
	}

//	Pr�ft, ob eine bestimmte Anzahl von Feldern rechts von einer
//	bestimmten Position sicher bewegt werden kann. (inklusiv die Position selbst)
	private boolean isSafeToTheRightOf(Position position, int squaresToCheck) {

		ArrayList<Position> list = new ArrayList<>();
		Team t = getTeam();

		for (int i = 0; i < squaresToCheck; i++) {
			list.add(new Position(position.getX() + i, position.getY()));
		}

		for (Position p : list) {
			if (!safeSpot(t, p)) {
				return false;
			}
		}

		return true;
	}

//	Gibt die unmittelbar umliegenden Positionen des K�nigs zur�ck.
	public ArrayList<Position> getSurroundingSquares(Position position) {
		ArrayList<Position> squares = new ArrayList<>();
		loadHorizontalAndVerticalSquares(position, squares);
		loadDiagonalSquares(position, squares);
		return squares;
	}

//	L�dt die benachbarten horizontalen und vertikalen Quadrate.
	private void loadHorizontalAndVerticalSquares(Position position, ArrayList<Position> squares) {
		Position up = new Position(position.getX(), position.getY() - 1);
		Position down = new Position(position.getX(), position.getY() + 1);
		Position left = new Position(position.getX() + 1, position.getY());
		Position right = new Position(position.getX() - 1, position.getY());

		if (isValidSpot(this, up)) {
			squares.add(up);
		}
		if (isValidSpot(this, down)) {
			squares.add(down);
		}
		if (isValidSpot(this, left)) {
			squares.add(left);
		}
		if (isValidSpot(this, right)) {
			squares.add(right);
		}
	}

//	L�dt die benachbarten diagonalen Quadrate.
	private void loadDiagonalSquares(Position position, ArrayList<Position> squares) {
		Position upLeft = new Position(position.getX() - 1, position.getY() - 1);
		Position upRight = new Position(position.getX() + 1, position.getY() - 1);
		Position downLeft = new Position(position.getX() - 1, position.getY() + 1);
		Position downRight = new Position(position.getX() + 1, position.getY() + 1);

		if (isValidSpot(this, upLeft)) {
			squares.add(upLeft);
		}
		if (isValidSpot(this, upRight)) {
			squares.add(upRight);
		}
		if (isValidSpot(this, downLeft)) {
			squares.add(downLeft);
		}
		if (isValidSpot(this, downRight)) {
			squares.add(downRight);
		}
	}

//	Erstellt ein neues identisches Objekt.
	public King clone(Board board) {
		King king = new King(board, getTeam());
		king.moved = this.moved;
		return king;
	}

	@Override
	public String toString() {
		return "King";
	}

}
