package com.games.chess.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.games.chess.entities.BoardUnit;
import com.games.chess.entities.GameState;
import com.games.chess.logic.Board;
import com.games.chess.logic.Position;
import com.games.chess.logic.Team;
import com.games.chess.logic.pieces.Piece;

public class GameStateBuildingService {

	public GameState buildGameState(String playerId, String gameId, Board board) {
		GameState gameState = new GameState();
		gameState.setPlayerTurn(board.getTurn());
		gameState.setGameOver(board.isGameOver());

		if (board.isGameOver()) {
			gameState.setWinner(board.getWinner());
		}
		List<BoardUnit> blackPieces = new ArrayList<>();
		List<BoardUnit> whitePieces = new ArrayList<>();

		Iterator<Position> boardIterator = board.iterator();
		while (boardIterator.hasNext()) {
			Position position = boardIterator.next();
			Piece piece = board.get(position);
			BoardUnit boardUnit = new BoardUnit(position.getX(), position.getY(), piece.toString());
			if (piece.getTeam().equals(Team.BLACK)) {
				blackPieces.add(boardUnit);
			} else {
				whitePieces.add(boardUnit);
			}
		}

		gameState.setBlackPieces(blackPieces);
		gameState.setWhitePieces(whitePieces);
		return gameState;
	}

}
