package com.games.chess.entities;

import java.util.concurrent.ConcurrentHashMap;

public class GamesRegister {

	private ConcurrentHashMap<String, ChessGame> registeredGames = new ConcurrentHashMap<>();

	public void reigisterGame(String gameId) {
		this.registeredGames.put(gameId, new ChessGame());
	}

	public void registerPlayer(String gameId, String playerId) {
		ChessGame chessGame = this.registeredGames.get(gameId);
		Player player = new Player(playerId);
		chessGame.registerPlayer(player);
	}

	public GameState getGameState(String gameId, String playerId) {
		ChessGame chessGame = this.registeredGames.get(gameId);
		GameState gameState = chessGame.getGameStateObject(gameId, playerId);
		return gameState;
	}

	public void makePlayerMove(String gameId, String playerId, int startX, int startY, int destinationX,
			int destinationY) {
		ChessGame chessGame = this.registeredGames.get(gameId);
		chessGame.makePlayerMove(gameId, playerId, startX, startY, destinationX, destinationY);
	}

}
