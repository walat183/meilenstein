package com.games.chess.entities;

import com.games.chess.logic.Team;

public class Player {

	private String playerId;
	private Team team;

	public Player(String playerId) {
		this.playerId = playerId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

}
