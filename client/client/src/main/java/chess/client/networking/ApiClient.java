package chess.client.networking;

import chess.client.entities.GameState;
import chess.client.entities.MoveRequest;
import chess.client.entities.MoveResponse;
import chess.client.entities.PlayerRegistration;

public interface ApiClient {

	String getGameId();

	PlayerRegistration getPlayerId(String gameId);

	GameState getGameState(String gameId, String playerId);

	MoveResponse postMove(String gameId, MoveRequest moveRequest);

}
