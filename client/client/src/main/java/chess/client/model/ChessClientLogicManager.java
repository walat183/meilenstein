package chess.client.model;

import java.util.Optional;

import chess.client.entities.BoardUnit;

public class ChessClientLogicManager {

	private Optional<BoardUnit> source;
	private Optional<BoardUnit> destination;

	public ChessClientLogicManager() {
		this.source = Optional.empty();
		this.destination = Optional.empty();
	}

	public boolean processClick(int x, int y) {
		if (source.isEmpty()) {
			source = Optional.of(new BoardUnit(x, y, ""));
			return false;
		}
		destination = Optional.of(new BoardUnit(x, y, ""));
		return true;
	}

	public BoardUnit getSource() {
		return source.get();
	}

	public BoardUnit getDestination() {
		return destination.get();
	}

	public void reset() {
		this.source = Optional.empty();
		this.destination = Optional.empty();
	}

}
