package chess.client.tasks;

import chess.client.entities.MoveRequest;
import chess.client.networking.ApiClient;
import javafx.concurrent.Task;

public class AsyncClientCallsManager {

	private ApiClient apiClient;

	public AsyncClientCallsManager(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	public Task<String> getNewGameIdRequestingTask() {
		return new NewGameIdTask(apiClient);
	}

	public RegisterPlayerIdTask getRegisterPlayerIdTask(String gameId) {
		return new RegisterPlayerIdTask(apiClient, gameId);
	}


}
