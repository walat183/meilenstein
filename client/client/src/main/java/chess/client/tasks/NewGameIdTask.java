package chess.client.tasks;

import chess.client.networking.ApiClient;
import javafx.concurrent.Task;

public class NewGameIdTask extends Task<String> {

	private ApiClient apiClient;

	public NewGameIdTask(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	@Override
	protected String call() throws Exception {
		return apiClient.getGameId();
	}

}
