package chess.client.tasks;

import chess.client.entities.PlayerRegistration;
import chess.client.networking.ApiClient;
import javafx.concurrent.Task;

public class RegisterPlayerIdTask extends Task<PlayerRegistration> {

	private ApiClient apiClient;
	private String gameId;

	public RegisterPlayerIdTask(ApiClient apiClient, String gameId) {
		this.apiClient = apiClient;
		this.gameId = gameId;
	}

	@Override
	protected PlayerRegistration call() throws Exception {
		return apiClient.getPlayerId(gameId);
	}

}
