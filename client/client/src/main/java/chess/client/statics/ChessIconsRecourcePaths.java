package chess.client.statics;

public class ChessIconsRecourcePaths {

	public static final String BLACK_BISHOP_ICON_PATH = "/ChessPiece/Black_Bishop.png";
	public static final String BLACK_KING_ICON_PATH = "/ChessPiece/Black_King.png";
	public static final String BLACK_KNIGHT_ICON_PATH = "/ChessPiece/Black_Knight.png";
	public static final String BLACK_PAWN_ICON_PATH = "/ChessPiece/Black_Pawn.png";
	public static final String BLACK_QUEEN_ICON_PATH = "/ChessPiece/Black_Queen.png";
	public static final String BLACK_ROOK_ICON_PATH = "/ChessPiece/Black_Rook.png";

	public static final String WHITE_BISHOP_ICON_PATH = "/ChessPiece/White_Bishop.png";
	public static final String WHITE_KING_ICON_PATH = "/ChessPiece/White_King.png";
	public static final String WHITE_KNIGHT_ICON_PATH = "/ChessPiece/White_Knight.png";
	public static final String WHITE_PAWN_ICON_PATH = "/ChessPiece/White_Pawn.png";
	public static final String WHITE_QUEEN_ICON_PATH = "/ChessPiece/White_Queen.png";
	public static final String WHITE_ROOK_ICON_PATH = "/ChessPiece/White_Rook.png";

}
