package chess.client.entities;

public class PlayerRegistration {

	private String playerId;
	private Team team;

	public PlayerRegistration(String playerId, Team team) {
		this.playerId = playerId;
		this.team = team;
	}

	public PlayerRegistration() {

	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

}
