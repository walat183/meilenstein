package chess.client.entities;

public class BoardUnit {

	private int x;
	private int y;
	private String pieceType;

	public BoardUnit(int x, int y, String pieceType) {
		this.x = x;
		this.y = y;
		this.pieceType = pieceType;
	}

	public BoardUnit() {
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getPieceType() {
		return pieceType;
	}

	public void setPieceType(String pieceType) {
		this.pieceType = pieceType;
	}

}