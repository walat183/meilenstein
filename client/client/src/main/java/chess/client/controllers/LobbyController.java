package chess.client.controllers;

import chess.client.viewmodel.LobbyViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

public class LobbyController {

	@FXML
	private Label newGameId;

	@FXML
	private PasswordField joinGameId;

	@FXML
	private Button createGame;

	@FXML
	private Button joinGame;

	private LobbyViewModel viewModel;

	public void init(LobbyViewModel viewModel) {
		this.viewModel = viewModel;
		newGameId.textProperty().bindBidirectional(viewModel.getNewGameIdProperty());
		joinGameId.textProperty().bindBidirectional(viewModel.getJoinGameIdProperty());
	}

	@FXML
	private void createGame(ActionEvent actionEvent) {
		viewModel.getNewGameId();
	}

	@FXML
	private void enterGame(ActionEvent actionEvent) {
		viewModel.joinGameById();
	}

}
