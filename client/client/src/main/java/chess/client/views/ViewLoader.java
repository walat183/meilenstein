package chess.client.views;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

import chess.client.controllers.LobbyController;
import chess.client.tasks.AsyncClientCallsManager;
import chess.client.viewmodel.LobbyViewModel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ViewLoader {

	private Stage primaryStage;
	private static Optional<ViewLoader> viewLoader;

	private ViewLoader(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	public static Optional<ViewLoader> getInstance() {
		return ViewLoader.viewLoader;
	}

	public static void setInstance(Stage primaryStage) {
		ViewLoader.viewLoader = Optional.of(new ViewLoader(primaryStage));
	}


	public void loadChessLobbyView(AsyncClientCallsManager asyncClientCallsManager) {

		try {
			ViewLoader.setInstance(primaryStage);
			LobbyViewModel viewModel = new LobbyViewModel(asyncClientCallsManager);
			FXMLLoader loader = new FXMLLoader();
			FileInputStream fileInputStream = new FileInputStream(
					new File("src/main/java/chess/client/views/lobby.fxml"));
			Parent vbox = loader.load(fileInputStream);
			LobbyController lobbyView = loader.getController();
			lobbyView.init(viewModel);
			primaryStage.setTitle("Chess Client");
			Scene scene = new Scene(vbox);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
