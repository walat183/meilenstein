package com.learning;

public class Radio {

    boolean istAn ;
    int lautsaerke;
    double frequenz;
    
    //konstruktor
    
    public Radio(boolean istAn, int lautsaerke, double frequenz) {

        this.istAn = istAn;

        if (lautsaerke >= 0 && lautsaerke <= 100) {
            this.lautsaerke = lautsaerke;
        }
        if (frequenz >= 85.0 && frequenz <= 110.0) {
            this.frequenz = frequenz;
        }
    }
    public void lauter () {
        if (istAn) {
            lautsaerke++;
        }
    }
    public void leiser () {
        if (istAn) {
            lautsaerke--;
        }
    }
    public boolean anschalten () {
          if (istAn = false) {
             istAn = true;
         }
        return istAn = true;
    }
    public boolean ausschalten () {

    	if (istAn = true) {
            istAn = false;
        }
        return istAn;
    }
    public String toString () {
        frequenz = 98.4;
        return "die frequenz :" + frequenz + " und die Aktuelle Lautstrke ist : " + lautsaerke;
    }
}